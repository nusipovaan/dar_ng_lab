import { map, catchError, mergeMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Category } from '@dar-lab-ng/api-interfaces';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'dar-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
 categories$: Observable<Category[]>;

 searchTerm = '';

  constructor(
    private httpClient: HttpClient,
    private router: Router
  ) { }

  ngOnInit(): void {
   this.getData();
  }
  getData() {
    this.categories$ = this.httpClient
    .get<Category[]>(`/api/categories`)
    .pipe(

      map(categories => this.searchTerm ?
        categories.filter(c=> c.title.toLocaleLowerCase().includes(this.searchTerm.toLocaleLowerCase()))
        : categories
      )
    )
  }
  modelChange(){
    console.log(this.searchTerm)
  }
  onSearchClick(){
    this.getData();
    console.log(this.searchTerm)
  }



}
