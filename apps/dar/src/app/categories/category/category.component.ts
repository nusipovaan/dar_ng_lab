import { tap } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Category } from './../../../../../../libs/api-interfaces/src/lib/api-interfaces';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dar-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
 category : Category;


 form: FormGroup;
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      sort: new FormControl('')
    });

    this.route.data.subscribe(data => {
      this.category = data.category;
      console.log(data.category)

    })
     this.form.patchValue(this.category)
  }
  onSubmit(){
    console.log(this.form.value);
    if(!this.form.valid){
      return;
    }
  }


}
