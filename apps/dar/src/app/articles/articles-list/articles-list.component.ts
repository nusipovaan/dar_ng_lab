import { Component, Input, OnInit , EventEmitter, Output} from '@angular/core';
import { Article } from '@dar-lab-ng/api-interfaces';
import {  } from 'events';

@Component({
  selector: 'dar-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.scss']
})
export class ArticlesListComponent implements OnInit {

  @Input()
  articles: Article[] =[];

  @Output()
  rowClicked = new EventEmitter<Article>();

  rowClickHandler(article: Article){
   
    this.rowClicked.emit(article);

  }

  constructor() { }

  ngOnInit(): void {
  }

}
