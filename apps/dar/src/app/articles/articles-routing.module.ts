import { ArticleResolver } from './article.resolver';
import { ArticlesComponent } from './articles/articles.component';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from "@angular/router";
import { ArticleComponent } from './article/article.component';

const routes: Route[] =[
    {
        path: '',
        component: ArticlesComponent,
    },
    {
        path: ':id',
        component: ArticleComponent,
        // resolve: {
        //   article: ArticleResolver
        // }
    }
]
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ArticlesRoutingModule{

}
