import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Article, Category } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import {catchError, switchMap, tap} from 'rxjs/operators'
@Component({
  selector: 'dar-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
 article: Article;
  article$: Observable<Article>;
  categories$: Observable<Category[]>

  form: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
      this.form = new FormGroup({
        title: new FormControl('', [Validators.required, Validators.minLength(3)]),
        annotation: new FormControl(''),
        is_published: new FormControl(false),
        category_id: new FormControl(''),
        tags: new FormArray([])
      });

      this.article$ = this.route.params
        .pipe(
          switchMap(params => this.http
            .get<Article>(`/api/articles/${params.id}`)),
          catchError(err => {
            console.error(err);
            return of(null);
          }),
          tap((article: Article | null) => {
            if (article) {
              this.form.patchValue(article);
              article.tags.forEach(tag => {
                const tagsArray = this.form.controls.tags as FormArray;
                const tagsControl =  new FormGroup({
                  name: new FormControl(tag.name)
                });
                tagsArray.push(tagsControl);
              })
            }
          })
        )

        // this.route.data.subscribe(data => {
        //   this.article$ = data.article;
        //   console.log(this.article$)

        // })



        this.categories$ =  this.http
        .get<Category[]>(`/api/categories`)
  }
  addTag(){
    const tagsArray = this.form.controls.tags as FormArray;
    const tagsControl =  new FormGroup({
      name: new FormControl('')
    });
    tagsArray.push(tagsControl);
  }
  onSubmit(){
    console.log(this.form.value)
    if(!this.form.valid){
      return;
    }
  }

}
